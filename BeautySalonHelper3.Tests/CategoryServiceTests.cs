﻿using BeautySalonHelper3.Models;
using BeautySalonHelper3.Services;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;

namespace BeautySalonHelper3.Tests
{
    [TestFixture]
    public class CategoryServiceTests
    {
        private BeautySalonHelperDbContext _context;
        private CategoryService _service;

        [SetUp]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<BeautySalonHelperDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(BeautySalonHelperDbContext))
                .Options;

            this._context = new BeautySalonHelperDbContext(options);
            _service = new CategoryService(_context);

        }

        [Test]

        public void AddCategory_ThrowsException_WhenReceivingNullObject()
        {
            Assert.Throws<ArgumentNullException>(() => _service.AddCategory(null));

        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]

        public void AddCategory_ThrowsValidationException_WhenNameIsInvalid(string name)
        {
            var category = CreateCategory();
            category.Name = name;

            Assert.Throws<ValidationException>(() => _service.AddCategory(category));

        }
        [Test]
        public void AddCategory_SavesNewCategory()
        {

            var category = CreateCategory();
            _service.AddCategory(category);
            Assert.AreEqual(1, _context.Categories.CountAsync().Result);

          


        }





        private Category CreateCategory()
        {
            return new Category
            {
                Name = "Category 1"
            };
        }

    }





}
