﻿using BeautySalonHelper3.Helpers;
using BeautySalonHelper3.Models;
using BeautySalonHelper3.Validation;
using BeautySalonHelper3.ViewModels;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;


namespace BeautySalonHelper3.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<UserGetModel> GetAll();
        void Register(RegisterPostModel registerInfo);
        User Update(int id, RegisterPostModel registerPostModel);
        User Delete(int id);
        void ChangeRole(string role, int id);

    }

    public class UserService : IUserService
    {

        private BeautySalonHelperDbContext context;
        private readonly AppSettings _appSettings;
        private RegisterPostModelValidator validations;


        public UserService(IOptions<AppSettings> appSettings, BeautySalonHelperDbContext context)
        {
            _appSettings = appSettings.Value;
            this.context = context;
            this.validations = new RegisterPostModelValidator();
        }

        public User Authenticate(string username, string password)
        {
            var user = context.Users.SingleOrDefault(x => x.Username == username && x.Password == ComputeSha256Hash(password));

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.role.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }
        private string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   

            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public void Register(RegisterPostModel registerInfo)
        {

            validations.Validate(registerInfo);
            User toAdd = new User
            {

                LastName = registerInfo.LastName,
                FirstName = registerInfo.FirstName,
                Password = ComputeSha256Hash(registerInfo.Password),
                Username = registerInfo.Username,
                role = Role.employee

            };
            context.Users.Add(toAdd);
            context.SaveChanges();
        }


        public IEnumerable<UserGetModel> GetAll()
        {
            // return users without passwords
            return context.Users.Select(user => new UserGetModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.Username,
                Role = user.role.ToString(),
                Token = null
            });


        }
        public void ChangeRole(string role, int id)
        {
            User user = context.Users.Select(x => x).Where(y => y.Id == id).FirstOrDefault();

            if (role != null)
            {
                if (role == "Admin")
                {
                    user.role = Role.admin;
                }
                if (role == "Employee")
                {
                    user.role = Role.employee;
                }
                context.Update(user);
                context.SaveChanges();
            }
        }

        public User Delete(int id)
        {
            var existing = context.Users

                .FirstOrDefault(user => user.Id == id);
            if (existing == null)
            {
                return null;
            }
            context.Users.Remove(existing);
            context.SaveChanges();
            return existing;
        }

        public User Update(int id, RegisterPostModel registerPostModel)
        {
            validations.Validate(registerPostModel);

            User user = new User();
            user.FirstName = registerPostModel.FirstName;
            user.LastName = registerPostModel.LastName;
            user.Password = ComputeSha256Hash(registerPostModel.Password);
            user.Username = registerPostModel.Username;

            user.Id = id;
            context.Users.Update(user);
            context.SaveChanges();
            return user;

        }


    }
}
