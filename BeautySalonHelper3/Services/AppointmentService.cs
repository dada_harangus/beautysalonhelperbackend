﻿using BeautySalonHelper3.Models;
using BeautySalonHelper3.Validation;
using BeautySalonHelper3.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;


namespace BeautySalonHelper3.Services
{
    public interface IAppointmentService
    {
        IEnumerable<AppointmentGetView> GetAll();
        Appointment GetById(int id);
        bool Upsert(int id, AppointmentAddModel appointment);
        Appointment Delete(int id);
        bool AddAppointment(AppointmentAddModel appointmentAddModel);
        IEnumerable<AppointmentGetView> GetAppointmentsByDate(string date);


    }
    public class AppointmentService : IAppointmentService
    {
        private AppointmentAddModelValidator validations;
        private BeautySalonHelperDbContext context;
        public AppointmentService(BeautySalonHelperDbContext context)
        {
            this.context = context;
            this.validations = new AppointmentAddModelValidator();
        }




        public Appointment Delete(int id)
        {
            var existing = context.Appointments

                .FirstOrDefault(appointment => appointment.Id == id);
            if (existing == null)
            {
                return null;
            }
            context.Appointments.Remove(existing);
            context.SaveChanges();
            return existing;
        }

        public IEnumerable<AppointmentGetView> GetAll()
        {
            IEnumerable<AppointmentGetView> result = context.Appointments.Select(x => new AppointmentGetView
            {
                Id = x.Id,
                Client = new ClientAddModel
                {
                    Id = x.Client.Id,
                    ClientName = x.Client.Name,
                    TelephoneNumber = x.Client.TelefonNumber,
                    Description = x.Client.Description,
                    Email = x.Client.Email
                },
                Employee = x.Employee,
                Services = x.AppointementService.Select(y => y.Service).ToList(),
                StartTime = x.StartTime,
                EndTime = x.EndTime

            }).ToList();


            return result;
        }


        public IEnumerable<AppointmentGetView> GetAppointmentsByDate(string date)
        {
            var format = "d";
            var provider = new CultureInfo("fr-FR");
            DateTime dateTime = DateTime.ParseExact(date, format, provider);
            //search for all appointments from a specific day
            IEnumerable<AppointmentGetView> result = context.Appointments.Select(x => new AppointmentGetView
            {
                Id = x.Id,
                Client = new ClientAddModel
                {
                    Id = x.Client.Id,
                    ClientName = x.Client.Name,
                    TelephoneNumber = x.Client.TelefonNumber,
                    Description = x.Client.Description,
                    Email = x.Client.Email
                },
                Employee = x.Employee,
                Services = x.AppointementService.Select(y => y.Service).ToList(),
                StartTime = x.StartTime,
                EndTime = x.EndTime
            }).Where(x => x.StartTime.Day == dateTime.Day).ToList();
            return result;
        }



        public Appointment GetById(int id)
        {
            return context.Appointments
                 .FirstOrDefault(e => e.Id == id);
        }

        public bool Upsert(int id, AppointmentAddModel appointment)
        {
            validations.ValidateAndThrow(appointment);
            var format = "g";
            var provider = new CultureInfo("fr-FR");
            var existing = context.Appointments.FirstOrDefault(f => f.Id == id);
            existing.Client = context.Clients.Find(appointment.Client.Id);
            existing.Employee = context.Employees.Find(appointment.Employee.Id);
            existing.AppointementService = new List<AppointmentServices>();

            foreach (var service in appointment.Services)
            {
                existing.AppointementService.Add(new AppointmentServices
                {
                    ServiceId = service.Id,
                    Service = service,
                    Appointment = existing
                });
            }
            
            existing.StartTime = DateTime.ParseExact(appointment.StartTime, format, provider);
            existing.EndTime = DateTime.ParseExact(appointment.EndTime, format, provider);
            
            Appointment appointmentExisting = context.Appointments.Select(x => x).Where(y => y.Employee == existing.Employee && y.StartTime == existing.StartTime).FirstOrDefault();
            if (appointmentExisting != null)
            {
                return false;
            }
            context.Appointments.Update(existing);
            context.SaveChanges();
            return true;
        }
        public bool AddAppointment(AppointmentAddModel appointmentAddModel)
        {
            validations.ValidateAndThrow(appointmentAddModel);

            Appointment appointmentNew = new Appointment();

            var format = "g";
            var provider = new CultureInfo("fr-FR");
            appointmentNew.Client = context.Clients.Find(appointmentAddModel.Client.Id);
            appointmentNew.Employee = context.Employees.Find(appointmentAddModel.Employee.Id);
            appointmentNew.AppointementService = new List<AppointmentServices>();

            foreach (var service in appointmentAddModel.Services)
            {
                appointmentNew.AppointementService.Add(new AppointmentServices
                {
                    ServiceId = service.Id,
                    //Service = service,
                    Appointment = appointmentNew
                });
            }
            appointmentNew.StartTime = DateTime.ParseExact(appointmentAddModel.StartTime, format, provider);
            appointmentNew.EndTime = DateTime.ParseExact(appointmentAddModel.EndTime, format, provider);
            
            Appointment appointmentExisting = context.Appointments.Select(x => x).Where(y => y.Employee == appointmentNew.Employee && y.StartTime == appointmentNew.StartTime).FirstOrDefault();
            if (appointmentExisting != null)
            {
                return false;
            }

            context.Appointments.Add(appointmentNew);
            context.SaveChanges();
            return true;

        }

    }
}