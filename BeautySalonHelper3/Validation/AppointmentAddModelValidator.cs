﻿using BeautySalonHelper3.ViewModels;
using FluentValidation;
using System;


namespace BeautySalonHelper3.Validation
{
    public class AppointmentAddModelValidator : AbstractValidator<AppointmentAddModel>
    {
        public AppointmentAddModelValidator()
        {
            RuleFor(appoitment => appoitment.Client).NotEmpty();
            RuleFor(appoitment => appoitment.Employee).NotEmpty();
            RuleFor(appoitment => appoitment.Services).NotEmpty();
            RuleFor(appoitment => appoitment.StartTime).Must(isValidDateTime).WithMessage("Hour must not be empty");
            RuleFor(appoitment => appoitment.EndTime).Must(isValidDateTime).WithMessage("Hour must not be empty");


        }

        private bool isValidDateTime(string dateTime)
        {
            var format = "g";
            var provider = new System.Globalization.CultureInfo("fr-FR");
            DateTime x;
            return DateTime.TryParseExact(dateTime, format, provider, System.Globalization.DateTimeStyles.None, out x);
        }
    }
}
