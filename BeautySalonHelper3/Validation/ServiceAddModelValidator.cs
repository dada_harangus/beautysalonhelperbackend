﻿using BeautySalonHelper3.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.Validation
{
    public class ServiceAddModelValidator: AbstractValidator<ServiceAddModel>
    {

        public ServiceAddModelValidator()
        {
            RuleFor(service => service.Name).NotEmpty();
            RuleFor(service => service.Price).NotEmpty();
            RuleFor(service => service.Duration).NotEmpty();


        }
    }
}
