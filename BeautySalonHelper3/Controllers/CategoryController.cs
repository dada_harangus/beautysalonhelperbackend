﻿using System.Collections.Generic;
using BeautySalonHelper3.Models;
using BeautySalonHelper3.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;

namespace BeautySalonHelper3.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private ICategoryService service;
        public CategoryController(ICategoryService service) 
        {
            this.service = service;
          
        }
        // GET: api/Category
        /// <summary>
        /// Get all the Category
        /// </summary>

        /// <returns>A list of Category objects</returns>
       
        [HttpGet]
        public IEnumerable<Category> Get()
        {

            return service.GetAll();

        }
        /// <summary>
        /// Saves a category
        /// </summary>
        /// <param name="category">The category to add</param>
        /// <returns>A category Object</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult AddCategory([FromBody] Category category)
        {
            try
            {
                var result = service.AddCategory(category);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });


            }
        }

        // GET: api/Category/5
        /// <summary>
        /// Get the Category that has the id requested
        /// </summary>
        /// <param name="id">The id of the Category</param>
        /// <returns>The Category with the given Id</returns>
        [HttpGet("{id}", Name = "GetCategory")]
        public IActionResult Get(int id)
        {
            var found = service.GetById(id);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }

        // PUT: api/Category/5
        /// <summary>
        /// Update the Category with the given id
        /// </summary>
        /// <param name="id">The id of the Category we want to update</param>
        /// <param name="service">The Category that contains the new data</param>
        /// <returns>An Category object</returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Update([FromQuery]int id, [FromBody] Category category)
        {
            try
            {
                var result = service.Update(id,category);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });


            }
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete the Category with the given id
        /// </summary>
        /// <param name="id">The id of the Category we want to delete</param>
        /// <returns>an Category object</returns>
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete([FromQuery]int id)
        {
            var existing = service.Delete(id);
           

            return Ok(existing);
        }
    }
}