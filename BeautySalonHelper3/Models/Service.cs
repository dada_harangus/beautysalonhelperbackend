﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.Models
{
    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Category Category { get; set; }
        public int Price { get; set; }
        public int Duration { get; set; }
        public List<AppointmentServices> AppointementService { get; set; }
    }
}
