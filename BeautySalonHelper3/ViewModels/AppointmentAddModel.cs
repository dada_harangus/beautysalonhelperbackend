﻿using BeautySalonHelper3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.ViewModels
{
    public class AppointmentAddModel
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public Employee Employee { get; set; }
        public IEnumerable <Service> Services { get; set; }
        public ClientAddModel Client { get; set; }
        
    }
}
