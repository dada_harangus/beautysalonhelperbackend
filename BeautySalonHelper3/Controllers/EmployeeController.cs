﻿using FluentValidation;
using System.Collections.Generic;
using BeautySalonHelper3.Services;
using BeautySalonHelper3.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BeautySalonHelper3.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private IEmployeeService service;
        public EmployeeController(IEmployeeService service)
        {
            this.service = service;

        }
        // GET: api/ Employee
        /// <summary>
        /// Get all the  Employee
        /// </summary>

        /// <returns>A list of  Employee objects</returns>
        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<EmployeeGetView> Get()
        {
            return service.GetAll();
        }

        /// <summary>
        /// Saves an employee
        /// </summary>
        /// <param name="employeeAddModel">The new data </param>
        /// <returns>An employeeAddModel object</returns>

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult AddEmployee([FromBody] EmployeeAddModel employeeAddModel)
        {
            try
            {
                var result = service.AddEmployee(employeeAddModel);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });


            }
        }
        /// <summary>
        /// Gets the money report on employee
        /// </summary>
        /// <param name="idEmployee">the id of the employee</param>
        /// <returns>A list of MonthReport objects</returns>
        [AllowAnonymous]
        [HttpGet("GetReportsByMonth")]
        public IActionResult GetReportsByMonth(int idEmployee)
        {
            var found = service.GetReportsByMonth(idEmployee);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }
        /// <summary>
        /// Get the past appointments for a specific employee
        /// </summary>
        /// <param name="id">The id of the employee</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetAppointments")]

        public IActionResult GetApppointments([FromQuery]int id)
        {
            var found = service.GetHistoryAppointments(id);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }
        /// <summary>
        /// Gets the future appointments for a specific employee
        /// </summary>
        /// <param name="id">The id of the employee</param>
        /// <returns>A list of appoinments</returns>
        [AllowAnonymous]
        [HttpGet("GetFutureAppointments")]

        public IActionResult GetFutureApppointments([FromQuery]int id)
        {
            var found = service.GetFutureAppointments(id);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }

        // GET: api/ Employee/5
        /// <summary>
        /// Get the  Employee that has the id requested
        /// </summary>
        /// <param name="id">The id of the  Employee</param>
        /// <returns>The  Employee with the given Id</returns>
        [AllowAnonymous]
        [HttpGet("{id}", Name = "Get Employee")]
        public IActionResult Get(int id)
        {
            var found = service.GetById(id);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }




        // PUT: api/ Employee/5
        /// <summary>
        /// Update the  Employee with the given id
        /// </summary>
        /// <param name="id">The id of the  Employee we want to update</param>
        /// <param name="service">The  Employee that contains the new data</param>
        /// <returns>An  Employee object</returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Upsert([FromQuery]int id, [FromBody]  EmployeeAddModel employee)
        {
            try
            {
                var result = service.Upsert(id, employee);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });


            }
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete the  Employee with the given id
        /// </summary>
        /// <param name="id">The id of the  Employee we want to delete</param>
        /// <returns>an  Employee object</returns>
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete([FromQuery]int id)
        {
            var existing = service.Delete(id);

            return Ok(existing);
        }
    }
}