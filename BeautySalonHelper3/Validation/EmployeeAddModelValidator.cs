﻿using BeautySalonHelper3.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.Validation
{
    public class EmployeeAddModelValidator:AbstractValidator<EmployeeAddModel>
    {
        public EmployeeAddModelValidator()
        {
            RuleFor(employee => employee.nameEmployee).NotEmpty();
            RuleFor(employee => employee.TelephoneNumber).NotEmpty().Matches(@"^\d{3}-\d{3}-\d{4}$").WithMessage("Please enter a valid phone number.");
            RuleFor(employee => employee.category).NotEmpty(); 
        }


    }
}
