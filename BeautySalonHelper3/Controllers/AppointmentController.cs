﻿using System.Collections.Generic;
using BeautySalonHelper3.Services;
using BeautySalonHelper3.ViewModels;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BeautySalonHelper3.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {
        private IAppointmentService service;
        public AppointmentController(IAppointmentService service)
        {
            this.service = service;

        }
        // GET: api/Appointment
        /// <summary>
        /// Get all the Appointment
        /// </summary>

        /// <returns>A list of Appointment objects</returns>
        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<AppointmentGetView> Get()
        {

            return service.GetAll();

        }
        /// <summary>
        /// Gets the appointments for a specific day
        /// </summary>
        /// <param name="date">The date</param>
        /// <returns>A list of appointments</returns>
        [HttpGet("GetAppointmentsByDate")]
        public IEnumerable<AppointmentGetView> GetAppointmentsByDate([FromQuery] string date)
        {

            return service.GetAppointmentsByDate(date);

        }

        // GET: api/Appointment/5
        /// <summary>
        /// Get the Appointment that has the id requested
        /// </summary>
        /// <param name="id">The id of the Appointment</param>
        /// <returns>The Appointment with the given Id</returns>
        [HttpGet("{id}", Name = "GetAppointment")]
        public IActionResult Get(int id)
        {
            var found = service.GetById(id);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }

        /// <summary>
        /// Saves an appointment
        /// </summary>
        /// <param name="appointmentAddModel">The appointment to save</param>
        /// <returns></returns>


        [HttpPost]

        public IActionResult AddAppointement([FromBody] AppointmentAddModel appointmentAddModel)
        {

            try
            {
                var result = service.AddAppointment(appointmentAddModel);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });
            }
        }



        // PUT: api/Appointment/5
        /// <summary>
        /// Update the Appointment with the given id
        /// </summary>
        /// <param name="id">The id of the Appointment we want to update</param>
        /// <param name="service">The Appointment that contains the new data</param>
        /// <returns>An Appointment object</returns>
        [HttpPut()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Upsert([FromQuery]int id, [FromBody] AppointmentAddModel appointment)
        {
            try
            {
                var result = service.Upsert(id, appointment);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });
            }
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete the Appointment with the given id
        /// </summary>
        /// <param name="id">The id of the Appointment we want to delete</param>
        /// <returns>an Appointment object</returns>
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult DeleteAppointment([FromQuery]int id)
        {
            var existing = service.Delete(id);
            if (existing == null)
            {
                return NotFound();
            }

            return Ok(existing);
        }
    }
}