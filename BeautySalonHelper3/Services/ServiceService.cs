﻿using BeautySalonHelper3.Models;
using BeautySalonHelper3.Validation;
using BeautySalonHelper3.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;

namespace BeautySalonHelper3.Services
{

    public interface IServiceService
    {
        IEnumerable<Service> GetAll();
        IEnumerable<Service> GetByCategory(int id);
        Service GetById(int id);
        ServiceAddModel Update(int id, ServiceAddModel service);
        bool Delete(int id);
        IEnumerable<Service> GetServiceByEmployee(int id);
        ServiceAddModel AddService(ServiceAddModel serviceAddModel);

    }
    public class ServiceService : IServiceService
    {
        private ServiceAddModelValidator serviceAddModelValidator;
        private BeautySalonHelperDbContext context;
        public ServiceService(BeautySalonHelperDbContext context)
        {
            this.serviceAddModelValidator = new ServiceAddModelValidator();
            this.context = context;
        }


        public IEnumerable<Service> GetAll()
        {
            IQueryable<Service> result = context.Services.OrderBy(f => f.Id);
            return result;
        }

        public bool Delete(int id)
        {

            bool boolean = true;
            //search to find any appointment with the service id
            var appointements = context.Appointments
                 .Include(x => x.AppointementService)
                 .ThenInclude(x => x.Service)
                 .Where(k => k.AppointementService.Any(s => s.ServiceId == id))
                 .FirstOrDefault();

            if (appointements != null)
            {
                boolean = false;
            }

            var existing = context.Services.FirstOrDefault(service => service.Id == id);
            if (existing == null)
            {
                boolean = false;
            }

            if (boolean == true)
            {
                context.Services.Remove(existing);
                context.SaveChanges();
            }
            return boolean;
        }


        public IEnumerable<Service> GetServiceByEmployee(int id)
        {
            //get the services available for an employee by searching the idCategory of the employee 
            //and the services with that idCategory
            var idCategory = context.Employees.Where(x => x.Id == id).Select(x => x.Category.Id).SingleOrDefault();
            var result = GetByCategory(idCategory);
            return result;

        }

        public Service GetById(int id)
        {
            return context.Services
                 .FirstOrDefault(e => e.Id == id);
        }



        public ServiceAddModel Update(int id, ServiceAddModel serviceAddModel)

        {
            serviceAddModelValidator.ValidateAndThrow(serviceAddModel);
            Service service = new Service();
            service.Name = serviceAddModel.Name;
            service.Duration = serviceAddModel.Duration;
            service.Price = serviceAddModel.Price;
            service.Category = context.Categories.SingleOrDefault(x => x.Id == serviceAddModel.CategoryId);
            service.Id = id;
            context.Services.Update(service);
            context.SaveChanges();
            return serviceAddModel;
        }

        public IEnumerable<Service> GetByCategory(int id)
        {
            //get all the services for a specific category
            var result = context.Services.Select(x => x).Where(x => x.Category.Id == id).ToList();
            return result;
        }


        public ServiceAddModel AddService(ServiceAddModel serviceAddModel)
        {
            serviceAddModelValidator.ValidateAndThrow(serviceAddModel);
            Service service = new Service();
            service.Name = serviceAddModel.Name;
            service.Duration = serviceAddModel.Duration;
            service.Price = serviceAddModel.Price;
            service.Category = context.Categories.SingleOrDefault(x => x.Id == serviceAddModel.CategoryId);
            context.Services.Add(service);
            context.SaveChanges();
            return serviceAddModel;
        }


    }
}
