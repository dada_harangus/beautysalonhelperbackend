﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.Models
{
    public class Appointment
    {
        public int Id { get; set; }
        public Client Client { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public Employee Employee { get; set; }
        public List<AppointmentServices> AppointementService { get; set; }
    }
}
