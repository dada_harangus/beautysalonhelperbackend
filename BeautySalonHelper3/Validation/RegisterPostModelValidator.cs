﻿using BeautySalonHelper3.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.Validation
{
    public class RegisterPostModelValidator:AbstractValidator<RegisterPostModel>
    {
       public RegisterPostModelValidator()
        {
            RuleFor(register => register.Username).NotEmpty();
            RuleFor(register => register.FirstName).NotEmpty();
            RuleFor(register => register.LastName).NotEmpty();
            RuleFor(register => register.Password).NotEmpty();
        }

    }
}
