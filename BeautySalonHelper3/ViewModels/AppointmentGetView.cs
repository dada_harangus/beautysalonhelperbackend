﻿using BeautySalonHelper3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.ViewModels
{
    public class AppointmentGetView
    {
        public int Id { get; set; }
        public ClientAddModel Client { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public Employee Employee { get; set; }
        public ICollection <Service> Services { get; set; }
    }
}
