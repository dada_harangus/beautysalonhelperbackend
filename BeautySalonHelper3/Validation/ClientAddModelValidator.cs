﻿using BeautySalonHelper3.ViewModels;
using FluentValidation;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.Validation
{
    public class ClientAddModelValidator : AbstractValidator<ClientAddModel>
    {
        public ClientAddModelValidator()
        {
            RuleFor(client => client.ClientName).NotEmpty();
            RuleFor(client => client.Email).NotEmpty().EmailAddress();
            RuleFor(client => client.TelephoneNumber).NotEmpty().Matches(@"^\d{3}-\d{3}-\d{4}$").WithMessage("Please enter a valid phone number.");


        }


    }





}
