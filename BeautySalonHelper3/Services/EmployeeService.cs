﻿using BeautySalonHelper3.Models;
using BeautySalonHelper3.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using BeautySalonHelper3.Validation;

namespace BeautySalonHelper3.Services
{
    public interface IEmployeeService
    {
        IEnumerable<AppointmentGetView> GetHistoryAppointments(int id);
        IEnumerable<AppointmentGetView> GetFutureAppointments(int id);
        IEnumerable<EmployeeGetView> GetAll();
        Employee GetById(int id);
        Employee Upsert(int id, EmployeeAddModel employee);
        bool Delete(int id);
        IEnumerable<MonthReports> GetReportsByMonth(int idEmployee);
        EmployeeAddModel AddEmployee(EmployeeAddModel employeeAddModel);

    }
    public class EmployeeService : IEmployeeService
    {
        private EmployeeAddModelValidator employeeAddModelValidator;
        private BeautySalonHelperDbContext context;
        public EmployeeService(BeautySalonHelperDbContext context)
        {
            this.employeeAddModelValidator = new EmployeeAddModelValidator();
            this.context = context;
        }


        public IEnumerable<MonthReports> GetReportsByMonth(int idEmployee)
        {
            DateTime today = DateTime.Now;


            IEnumerable<AppointmentGetView> result = context.Appointments.Select(x => new AppointmentGetView
            {
                //search all the past appoinments for the employee
                Id = x.Id,
                Client = new ClientAddModel
                {
                    Id = x.Client.Id,
                    ClientName = x.Client.Name,
                    TelephoneNumber = x.Client.TelefonNumber,
                    Description = x.Client.Description,
                    Email = x.Client.Email
                },
                Employee = x.Employee,
                Services = x.AppointementService.Select(y => y.Service).ToList(),
                StartTime = x.StartTime,
                EndTime = x.EndTime
            }).Where(x => x.Employee.Id == idEmployee && x.StartTime < today).ToList();

            // group and sum all the services prices by month
            var data = result.GroupBy(x => x.StartTime.Month).Select(x => new MonthReports
            {
                Month = x.Key,
                Sum = x.Sum(y => y.Services.Sum(k => k.Price))

            }).ToList();
            return data;
        }


        public bool Delete(int id)
        {

            var boolean = true;
            var existing = context.Employees

                .FirstOrDefault(employee => employee.Id == id);
            if (existing == null)
            {
                boolean = false;
            }
            //search if the employee has any appointments
            var appointement = context.Appointments.Select(x => x).Where(y => y.Employee.Id == id).FirstOrDefault();
            if (appointement != null)
            {
                boolean = false;
            }
            if (boolean == true)
            {
                context.Employees.Remove(existing);
                context.SaveChanges();
            }
            return boolean;
        }
        public IEnumerable<AppointmentGetView> GetHistoryAppointments(int id)
        {
            DateTime today = DateTime.Now;
            //search all the past appointments for the employee
            var result = context.Appointments.Select(x => new AppointmentGetView
            {
                Id = x.Id,
                Client = new ClientAddModel
                {
                    Id = x.Client.Id,
                    ClientName = x.Client.Name,
                    TelephoneNumber = x.Client.TelefonNumber,
                    Description = x.Client.Description,
                    Email = x.Client.Email
                },
                Employee = x.Employee,
                StartTime = x.StartTime,
                EndTime = x.EndTime,
                Services = x.AppointementService.Select(y => y.Service).ToList(),
            }).Where(x => x.Employee.Id == id && x.EndTime < today).ToList();
            return result;


        }

        public IEnumerable<AppointmentGetView> GetFutureAppointments(int id)
        {

            //search all the future appointments
            DateTime today = DateTime.Now;
            var result = context.Appointments.Select(x => new AppointmentGetView
            {
                Id = x.Id,
                Client = new ClientAddModel
                {
                    Id = x.Client.Id,
                    ClientName = x.Client.Name,
                    TelephoneNumber = x.Client.TelefonNumber,
                    Description = x.Client.Description,
                    Email = x.Client.Email
                },
                Employee = x.Employee,
                StartTime = x.StartTime,
                EndTime = x.EndTime,
                Services = x.AppointementService.Select(y => y.Service).ToList(),
            }).Where(x => x.Employee.Id == id && x.StartTime > today).ToList();
            return result;


        }
        public EmployeeAddModel AddEmployee(EmployeeAddModel employeeAddModel)
        {
            employeeAddModelValidator.ValidateAndThrow(employeeAddModel);
            Employee employee = new Employee();
            employee.Name = employeeAddModel.nameEmployee;
            employee.TelephoneNumber = employeeAddModel.TelephoneNumber;
            employee.Category = context.Categories.Where(x => x.Id == employeeAddModel.category).FirstOrDefault();
            context.Employees.Add(employee);
            context.SaveChanges();
            return employeeAddModel;


        }



        public IEnumerable<EmployeeGetView> GetAll()
        {

            IEnumerable<EmployeeGetView> result = context.Employees.Select(x => new EmployeeGetView
            {
                Id = x.Id,
                Name = x.Name,
                Category = x.Category
            }).ToList();


            return result;
        }



        public Employee GetById(int id)
        {
            var employeeNew = new Employee();
            employeeNew.Id = context.Employees.Where(x => x.Id == id).Select(x => x.Id).SingleOrDefault();
            employeeNew.Name = context.Employees.Where(x => x.Id == id).SingleOrDefault()?.Name;
            employeeNew.Category = context.Employees.Where(x => x.Id == id).Select(x => x.Category).SingleOrDefault();
            return employeeNew;
        }

        public Employee Upsert(int id, EmployeeAddModel employee)

        {
            employeeAddModelValidator.ValidateAndThrow(employee);
            var employeeNew = new Employee();
            employeeNew.Name = employee.nameEmployee;
            employeeNew.Category = context.Categories.Where(x => x.Id == employee.category).FirstOrDefault();
            employeeNew.Id = id;
            context.Employees.Update(employeeNew);
            context.SaveChanges();
            return employeeNew;
        }
    }
}
