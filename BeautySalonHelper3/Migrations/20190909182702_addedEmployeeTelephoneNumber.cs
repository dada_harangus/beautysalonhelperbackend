﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BeautySalonHelper3.Migrations
{
    public partial class addedEmployeeTelephoneNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TelephoneNumber",
                table: "Employees",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TelephoneNumber",
                table: "Employees");
        }
    }
}
