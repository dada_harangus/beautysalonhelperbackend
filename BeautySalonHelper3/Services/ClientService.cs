﻿using BeautySalonHelper3.Models;
using BeautySalonHelper3.Validation;
using BeautySalonHelper3.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;


namespace BeautySalonHelper3.Services
{
    public interface IClientService
    {
        IEnumerable<ClientAddModel> GetAll();
        IEnumerable<AppointmentGetView> GetAppointments(int id);
        IEnumerable<AppointmentGetView> GetFutureAppointments(int id);
        ClientAddModel GetById(int id);
        Client Update(int id, ClientAddModel client);
        bool Delete(int id);
        IEnumerable<MonthReports> GetReportsByMonth(int idClient);
        ClientAddModel AddClient(ClientAddModel clientAddModel);
        IEnumerable<ClientAddModel> Search(string name);

    }
    public class ClientService : IClientService
    {
        private BeautySalonHelperDbContext context;
        private ClientAddModelValidator validations;
        public ClientService(BeautySalonHelperDbContext context)
        {
            this.validations = new ClientAddModelValidator();
            this.context = context;
        }

        public IEnumerable<MonthReports> GetReportsByMonth(int idClient)
        {
            /// search all the past appointments
            DateTime today = DateTime.Now;
            IEnumerable<AppointmentGetView> result = context.Appointments.Select(x => new AppointmentGetView
            {
                Id = x.Id,
                Client = new ClientAddModel
                {
                    Id = x.Client.Id,
                    ClientName = x.Client.Name,
                    TelephoneNumber = x.Client.TelefonNumber,
                    Description = x.Client.Description,
                    Email = x.Client.Email
                },
                Employee = x.Employee,
                Services = x.AppointementService.Select(y => y.Service).ToList(),
                StartTime = x.StartTime,
                EndTime = x.EndTime
            }).Where(x => x.Client.Id == idClient && x.StartTime < today).ToList();

            //group and sum the prices of the services by month
            var data = result.GroupBy(x => x.StartTime.Month).Select(x => new MonthReports
            {
                Month = x.Key,
                Sum = x.Sum(y => y.Services.Sum(k => k.Price))

            }).ToList();
            return data;
        }

        //search by client name
        public IEnumerable<ClientAddModel> Search(string name)
        {
            var result = context.Clients.Select(x => new ClientAddModel
            {
                Id = x.Id,
                ClientName = x.Name,
                Email = x.Email,
                TelephoneNumber = x.TelefonNumber,
                Description = x.Description

            }).Where(y => y.ClientName.Contains(name)).ToList();

            return result;
        }


        //get past appointments
        public IEnumerable<AppointmentGetView> GetAppointments(int id)
        {
            DateTime today = DateTime.Now;

            var result = context.Appointments.Select(x => new AppointmentGetView
            {
                Id = x.Id,
                Client = new ClientAddModel
                {
                    Id = x.Client.Id,
                    ClientName = x.Client.Name,
                    TelephoneNumber = x.Client.TelefonNumber,
                    Description = x.Client.Description,
                    Email = x.Client.Email
                },
                Employee = x.Employee,
                StartTime = x.StartTime,
                EndTime = x.EndTime,
                Services = x.AppointementService.Select(y => y.Service).ToList(),

            }).Where(x => x.Client.Id == id && x.EndTime < today).ToList();
            return result;
        }
        //get future appointments
        public IEnumerable<AppointmentGetView> GetFutureAppointments(int id)
        {
            DateTime today = DateTime.Now;
            var result = context.Appointments.Select(x => new AppointmentGetView
            {
                Id = x.Id,
                Client = new ClientAddModel
                {
                    Id = x.Client.Id,
                    ClientName = x.Client.Name,
                    TelephoneNumber = x.Client.TelefonNumber,
                    Description = x.Client.Description,
                    Email = x.Client.Email
                },
                Employee = x.Employee,
                StartTime = x.StartTime,
                EndTime = x.EndTime,
                Services = x.AppointementService.Select(y => y.Service).ToList(),

            }).Where(x => x.Client.Id == id && x.StartTime > today).ToList();
            return result;
        }



        public IEnumerable<ClientAddModel> GetAll()
        {
            IEnumerable<ClientAddModel> result = context.Clients.Select(x => new ClientAddModel
            {
                Id = x.Id,
                ClientName = x.Name,
                Email = x.Email,
                TelephoneNumber = x.TelefonNumber,
                Description = x.Description

            }).ToList();
            return result;
        }



        public ClientAddModel GetById(int id)
        {
            return context.Clients.Select(x => new ClientAddModel
            {
                Id = x.Id,
                TelephoneNumber = x.TelefonNumber,
                ClientName = x.Name,
                Email = x.Email,
                Description = x.Description

            }).Where(x => x.Id == id).FirstOrDefault();
        }

        public Client Update(int id, ClientAddModel clientAddModel)
        {
            validations.ValidateAndThrow(clientAddModel);
            Client client = new Client();
            client.Id = clientAddModel.Id;
            client.Name = clientAddModel.ClientName;
            client.TelefonNumber = clientAddModel.TelephoneNumber;
            client.Email = clientAddModel.Email;
            client.Description = clientAddModel.Description;
            client.Id = id;
            context.Clients.Update(client);
            context.SaveChanges();
            return client;
        }


        public ClientAddModel AddClient(ClientAddModel clientAddModel)
        {

            validations.ValidateAndThrow(clientAddModel);
            Client client = new Client();
            client.Name = clientAddModel.ClientName;
            client.Email = clientAddModel.Email;
            client.TelefonNumber = clientAddModel.TelephoneNumber;
            client.Description = clientAddModel.Description;
            context.Clients.Add(client);
            context.SaveChanges();
            return clientAddModel;


        }

        public bool Delete(int id)
        {
            var boolean = true;
            var existing = context.Clients.FirstOrDefault(client => client.Id == id);
            //search if the client has any appointments
            var appointment = context.Appointments.Select(x => x).Where(y => y.Client.Id == id).FirstOrDefault();

            if (appointment != null)
            {
                boolean = false;
            }
            if (existing == null)
            {
                boolean = false;
            }
            if (boolean == true)
            {
                context.Clients.Remove(existing);
                context.SaveChanges();
            }
            return boolean;
        }


    }
}
