﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.ViewModels
{
    public class EmployeeAddModel
    {
        public int Id { get; set; }
        public string nameEmployee { get; set; }
        public string TelephoneNumber { get; set; }
        public int category { get; set; }
    }
}
