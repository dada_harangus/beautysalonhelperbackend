﻿using FluentValidation;
using System.Collections.Generic;
using BeautySalonHelper3.Models;
using BeautySalonHelper3.Services;
using BeautySalonHelper3.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BeautySalonHelper3.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private IServiceService service;
        public ServiceController(IServiceService service)
        {
            this.service = service;

        }
        // GET: api/Service
        /// <summary>
        /// Get all the services
        /// </summary>

        /// <returns>A list of Service objects</returns>

        [HttpGet]
        public IEnumerable<Service> Get()
        {

            return service.GetAll();

        }
        /// <summary>
        /// Get all the services for a category
        /// </summary>
        /// <param name="id"> id of the category</param>
        /// <returns>A list of services</returns>
        [HttpGet("GetByCategory")]
        public IEnumerable<Service> GetByCategory([FromQuery] int id)
        {

            return service.GetByCategory(id);

        }

        /// <summary>
        /// Gets the services available for an employee
        /// </summary>
        /// <param name="id">The employee id </param>
        /// <returns>A list of services</returns>
        [HttpGet("GetServicesByEmployee")]
        public IEnumerable<Service> GetServicesByEmployee([FromQuery] int id)
        {

            return service.GetServiceByEmployee(id);

        }

        // GET: api/Service/5
        /// <summary>
        /// Get the Service that has the id requested
        /// </summary>
        /// <param name="id">The id of the Service</param>
        /// <returns>The Service with the given Id</returns>
        [HttpGet("{id}", Name = "GetService")]
        public IActionResult Get(int id)
        {
            var found = service.GetById(id);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }
        // Post: api/Service/
        /// <summary>
        /// Add an Service 
        /// </summary>
        /// 
        /// <param name="serviceAddModel">The Service that contains the new data</param>
        /// <returns>An Service object</returns>

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult AddService([FromBody] ServiceAddModel serviceAddModel)
        {
            try
            {
                var result = service.AddService(serviceAddModel);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });


            }
        }

        // PUT: api/Service/5
        /// <summary>
        /// Update the Service with the given id
        /// </summary>
        /// <param name="id">The id of the Service we want to update</param>
        /// <param name="service">The Service that contains the new data</param>
        /// <returns>An Service object</returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Update([FromQuery]int id, [FromBody] ServiceAddModel serviceParam)
        {
            try
            {
                var result = service.Update(id, serviceParam);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });


            }
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete the Service with the given id
        /// </summary>
        /// <param name="id">The id of the Service we want to delete</param>
        /// <returns>an Service object</returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult DeleteService([FromQuery]int id)
        {
            var existing = service.Delete(id);


            return Ok(existing);
        }
    }
}