﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.ViewModels
{
    public class MonthReports
    {

        public int Id { get; set; }
        public int Month { get; set; }
        public int Sum { get; set; }
    }
}
