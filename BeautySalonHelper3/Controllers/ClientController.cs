﻿
using System.Collections.Generic;
using BeautySalonHelper3.Services;
using BeautySalonHelper3.ViewModels;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BeautySalonHelper3.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private IClientService service;
        public ClientController(IClientService service)
        {
            this.service = service;

        }
        // GET: api/Client
        /// <summary>
        /// Get all the Client
        /// </summary>
        /// <returns>A list of Client objects</returns>
        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<ClientAddModel> Get()
        {

            return service.GetAll();

        }
        /// <summary>
        /// Gets a money report for a specific client
        /// </summary>
        /// <param name="idClient">The client id</param>
        /// <returns>A list of MonthReport objects</returns>
        [HttpGet("GetReportsByMonth")]
        public IActionResult GetReportsByMonth(int idClient)
        {
            var found = service.GetReportsByMonth(idClient);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }

        // GET: api/Client/5
        /// <summary>
        /// Get the Client that has the id requested
        /// </summary>
        /// <param name="id">The id of the Client</param>
        /// <returns>The Client with the given Id</returns>
        [HttpGet("{id}", Name = "GetClient")]
        public IActionResult Get(int id)
        {
            var found = service.GetById(id);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }
        /// <summary>
        /// Saves a client
        /// </summary>
        /// <param name="clientAddModel">The new client</param>
        /// <returns>A client objects</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult AddClient([FromBody] ClientAddModel clientAddModel)
        {
            try
            {
                var result = service.AddClient(clientAddModel);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });
            }
        }

        // PUT: api/Client/5
        /// <summary>
        /// Update the Client with the given id
        /// </summary>
        /// <param name="id">The id of the Client we want to update</param>
        /// <param name="service">The Client that contains the new data</param>
        /// <returns>An Client object</returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpsertClient([FromQuery]int id, [FromBody] ClientAddModel client)
        {
            try
            {
                var result = service.Update(id, client);
                return Ok(result);
            }
            catch (ValidationException validationEx)
            {
                return BadRequest(new { message = validationEx.Errors });


            }
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete the Client with the given id
        /// </summary>
        /// <param name="id">The id of the Client we want to delete</param>
        /// <returns>an Client object</returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult DeleteClient([FromQuery]int id)
        {
            var existing = service.Delete(id);

            return Ok(existing);
        }

        /// <summary>
        /// Get past appointments
        /// </summary>
        /// <param name="id">The id of the client</param>
        /// <returns>A list of appointments</returns>
        [HttpGet("GetAppointments")]

        public IActionResult GetApppointments([FromQuery]int id)
        {
            var found = service.GetAppointments(id);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }
        /// <summary>
        /// Search by name
        /// </summary>
        /// <param name="name">The name for which we are searching</param>
        /// <returns>A list of clients</returns>
        [HttpGet("Search")]

        public IActionResult Search([FromQuery]string name)
        {
            var found = service.Search(name);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }
        /// <summary>
        /// Get the future appointments
        /// </summary>
        /// <param name="id">The id of the client</param>
        /// <returns>A list of appointments</returns>
        [HttpGet("GetFutureAppointments")]

        public IActionResult GetFutureApppointments([FromQuery]int id)
        {
            var found = service.GetFutureAppointments(id);

            if (found == null)
            {
                return NotFound();
            }

            return Ok(found);
        }

    }
}