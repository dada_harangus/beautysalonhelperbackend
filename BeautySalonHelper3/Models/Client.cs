﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TelefonNumber { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }

    }
}
