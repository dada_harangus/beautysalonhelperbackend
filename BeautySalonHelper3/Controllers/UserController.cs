﻿using BeautySalonHelper3.Models;
using BeautySalonHelper3.Services;
using BeautySalonHelper3.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BeautySalonHelper3.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]


    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]User userParam)
        {
            var user = _userService.Authenticate(userParam.Username, userParam.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }
        [HttpGet("ChangeRole")]
        public IActionResult ChangeRole([FromQuery] int id, [FromQuery] string role)
        {
            _userService.ChangeRole(role, id);
            return Ok();

        }


        [AllowAnonymous]
        [HttpPost("register")]
        //[HttpPost]
        public IActionResult Register([FromBody]RegisterPostModel registerModel)
        {
            _userService.Register(registerModel);//, out User user);

            return Ok();//user);
        }
    }
}