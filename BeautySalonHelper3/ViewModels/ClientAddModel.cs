﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeautySalonHelper3.ViewModels
{
    public class ClientAddModel
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string TelephoneNumber { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
    }
}
