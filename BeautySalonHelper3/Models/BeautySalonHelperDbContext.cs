﻿using Microsoft.EntityFrameworkCore;

namespace BeautySalonHelper3.Models
{
    public class BeautySalonHelperDbContext : DbContext
    {
        public BeautySalonHelperDbContext(DbContextOptions<BeautySalonHelperDbContext> options) : base(options)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>(entity =>
            {
                entity.HasIndex(u => u.Username).IsUnique();
            });

        }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
