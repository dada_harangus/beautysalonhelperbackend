﻿using BeautySalonHelper3.Models;
using BeautySalonHelper3.Validation;
using Microsoft.EntityFrameworkCore;
using FluentValidation;
using System.Collections.Generic;
using System.Linq;


namespace BeautySalonHelper3.Services
{
    public interface ICategoryService
    {
        IEnumerable<Category> GetAll();
        Category GetById(int id);
        Category Update(int id, Category category);
        bool Delete(int id);
        Category AddCategory(Category category);

    }
    public class CategoryService : ICategoryService
    {
        private BeautySalonHelperDbContext context;
        private CategoryValidator categoryValidator;
        public CategoryService(BeautySalonHelperDbContext context)
        {
            this.categoryValidator = new CategoryValidator();
            this.context = context;
        }

        public IEnumerable<Category> GetAll()
        {
            IQueryable<Category> result = context.Categories.Select(x => x);
            return result;
        }

        public Category GetById(int id)
        {
            return context.Categories
                 .FirstOrDefault(e => e.Id == id);
        }

        public Category Update(int id, Category category)
        {
            categoryValidator.ValidateAndThrow(category);
            var existing = context.Categories.AsNoTracking().FirstOrDefault(f => f.Id == id);
            existing.Name = category.Name;
            context.Categories.Update(existing);
            context.SaveChanges();
            return category;
        }

        public Category AddCategory(Category category)
        {
            categoryValidator.ValidateAndThrow(category);
            context.Categories.Add(category);
            context.SaveChanges();
            return category;
        }


        public bool Delete(int id)
        {
            var boolean = true;
            //search for an employee or a services with this category
            Employee employee = context.Employees.Select(x => x).Where(y => y.Category.Id == id).FirstOrDefault();
            Service services = context.Services.Select(x => x).Where(y => y.Category.Id == id).FirstOrDefault();

            if (employee != null)
            {

                boolean = false;
            }

            if (services != null)
            {

                boolean = false;
            }
            var existing = context.Categories

                .FirstOrDefault(category => category.Id == id);
            if (existing == null)
            {
                boolean = false;
            }
            if (boolean == true)
            {
                context.Categories.Remove(existing);
                context.SaveChanges();
            }
            return boolean;
        }


    }
}
